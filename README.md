## Yet Another Linked List Implementation

A generic linked list implementation in the Linux container_of() style with emphasis on embedded applications. Built with meson and a Unity test suite.