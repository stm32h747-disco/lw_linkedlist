
#include "utils_linkedlist.h"

#ifndef DBL_LINKEDLIST_H
# define DBL_LINKEDLIST_H

typedef struct dll_node
{
    struct dll_node * next;
    struct dll_node * previous;

} dll_node_t, *pdll_node_t;

typedef struct
{
    pdll_node_t head;
    pdll_node_t tail;
} dll_root_t, *pdll_root_t;

ll_status_t dbl_linkedlist_node_insert(pdll_root_t linkedlist, pdll_node_t new_node, uint32_t index);

ll_status_t dbl_linkedlist_node_insert_precise(pdll_root_t linkedlist, pdll_node_t new_node, uint32_t index);

ll_status_t dbl_linkedlist_node_insert_head(pdll_root_t linkedlist, pdll_node_t new_node);

ll_status_t dbl_linkedlist_node_insert_tail(pdll_root_t linkedlist, pdll_node_t new_node);

#endif  /* DBL_LINKEDLIST_H */