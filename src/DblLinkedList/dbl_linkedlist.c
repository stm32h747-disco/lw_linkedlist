
#include "dbl_linkedlist.h"

static inline ll_status_t dbl_linkedlist_node_insert_impl(pdll_root_t linkedlist, pdll_node_t new_node, uint32_t index)
{
    pdll_node_t node_before;

    node_before = linkedlist->head;

    if (index == 0)
    {
        new_node->next      = linkedlist->head;
        new_node->previous  = NULL;
        linkedlist->head    = new_node;
        goto ok;
    }

    LINKEDLIST_TRAVERSE_UNTIL(--index, node_before);

#ifdef LL_SAFE_INSERT
    if (node_before->next == NULL && index != 0)
    {
        return LL_ERROUTSIDERANGE;
    }
#endif

    new_node->next        = node_before->next;
    new_node->previous    = node_before;
    node_before->previous = new_node;
    node_before->next     = new_node;

ok:
    return LL_OK;
}

ll_status_t dbl_linkedlist_node_insert_head(pdll_root_t linkedlist, pdll_node_t new_node)
{
    new_node->next     = linkedlist->head;
    new_node->previous = NULL;
    linkedlist->head   = new_node;

    return LL_OK;
}

ll_status_t dbl_linkedlist_node_insert_tail(pdll_root_t linkedlist, pdll_node_t new_node)
{
    new_node->next     = NULL;
    new_node->previous = linkedlist->tail;
    linkedlist->tail   = new_node;

    return LL_OK;
}

ll_status_t dbl_linkedlist_node_remove(pdll_node_t node)
{
    (node->previous)->next = node->next;
    (node->next)->previous = node->previous;

    return LL_OK;
}
