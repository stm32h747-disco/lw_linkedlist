#include "linkedlist.h"

#define LL_SAFE_INSERT
#include "linkedlist_impl.h"


/**
 * \brief           Insert a new linkedlist node at the given index. Guarantees
 *                  the node will be found at the specified index otherwise fails.
 * \param[in]       linkedlist: Linkedlist head handle
 * \param[in]       new_node: Pointer to node to insert
 * \param[in]       index: Index to insert the node. Index must be within the
 *                      size of the Linkedlist.
 * \return          LL_OK on success, LL_ERROUTSIDERANGE if the end of the 
 *                  linkedlist is readched before the index.
 */
ll_status_t linkedlist_node_insert_precise(pll_node_t linkedlist, pll_node_t new_node, uint32_t index)
{
    return linkedlist_node_insert_impl(linkedlist, new_node, index);
}