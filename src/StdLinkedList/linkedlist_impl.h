
#include "linkedlist.h"

#undef LL_DBL

// TODO: Fully implement circular mode
// TODO: Implement double head with conditional compilation
static inline __attribute__ ((always_inline))
ll_status_t linkedlist_node_insert_impl(pll_node_t head, pll_node_t new_node, uint32_t index)
{
    pll_node_t pnode;

    pnode = head;

    LINKEDLIST_TRAVERSE_UNTIL(index--, pnode);

#ifdef LL_SAFE_INSERT
    if (pnode->next == NULL && (++index) != 0)
    {
        return LL_ERR_OUTSIDERANGE;
    }
#endif

    new_node->next = pnode->next;
#ifdef LL_DBL
    new_node->previous = (*ppnode_next)->previous;
    (*ppnode_next)->previous = new_node;
#endif
    pnode->next = new_node;

#ifdef LL_CIRCULAR
    if (new_node->next == NULL)
    {
        new_node->next = head->next;
    }
#endif

    return LL_OK;
}

static inline __attribute__ ((always_inline))
ll_status_t linkedlist_node_remove_index_impl(pll_node_t head, uint32_t index)
{
    pll_node_t pnode, pnode_remove;

    pnode = head;

    if (index)
    {
        LINKEDLIST_TRAVERSE_UNTIL(index--, pnode);
    }

    if (pnode->next == NULL)
    {
        return LL_ERR_OUTSIDERANGE;
    }

#ifdef LL_DBL
    new_node->previous = (*ppnode_next)->previous;
    (*ppnode_next)->previous = new_node;
#endif
    pnode_remove = pnode->next;
    pnode->next = pnode->next->next;
    pnode_remove->next = NULL;

    return LL_OK;
}

static inline __attribute__ ((always_inline))
ll_status_t linkedlist_node_remove_impl(pll_node_t * pnode)
{
    pll_node_t pnode_before, pnode_remove;

    pnode_before = container_of(pnode, ll_node_t, next);

    // node {
    //   node * next;      
    // }

    pnode_remove = pnode_before->next;
    pnode_before->next = (*pnode)->next;
    (*pnode)->next = NULL;

    return LL_OK;
}

