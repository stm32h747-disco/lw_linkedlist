
#include "utils_linkedlist.h"

#ifndef LINKEDLIST_H
# define LINKEDLIST_H

#ifdef LL_SAFE_INSERT
# warning "LL_SAFE_INSERT has been defined globally. It will be undefined in the compilation process"
#endif

typedef struct ll_node
{
    struct ll_node * next;
} ll_node_t, *pll_node_t;

typedef struct
{
    pll_node_t head;
    // TODO: modify implementation to use
    // root node for queue optimisation
#ifdef LL_QUEUE_OPTIMISE
    pll_node_t tail;
#endif
} ll_root_t, *pll_root_t;

#define GET_NEXT(pnode) (pnode & (UINT32_MAX - 1))
#define NODE_DELETED(pnode) (pnode & (1 << 0))


ll_status_t linkedlist_node_insert(pll_node_t linkedlist, pll_node_t new_node, uint32_t index);

ll_status_t linkedlist_node_insert_precise(pll_node_t linkedlist, pll_node_t new_node, uint32_t index);

ll_status_t linkedlist_node_insert_head(pll_node_t linkedlist, pll_node_t new_node);

ll_status_t linkedlist_node_insert_tail(pll_node_t linkedlist, pll_node_t new_node);

ll_status_t linkedlist_node_remove_index(pll_node_t head, uint32_t index);

ll_status_t linkedlist_node_remove(pll_node_t * pnode);

ll_status_t linkedlist_length(pll_node_t linkedlist, uint32_t * size);

ll_status_t linkedlist_node_get_index(pll_node_t linkedlist, pll_node_t * pnode, uint32_t index);

#endif  /* LINKEDLIST_H */