
#include "linkedlist.h"

ll_status_t linkedlist_node_insert_head(pll_node_t linkedlist, pll_node_t new_node)
{
    new_node->next = linkedlist->next;
    linkedlist->next = new_node;

    return LL_OK;
}

ll_status_t linkedlist_node_insert_tail(pll_node_t linkedlist, pll_node_t new_node)
{
    pll_node_t pnode;

    pnode = linkedlist->next;

    //ASSERT(linkedlist->head != NULL);

#ifdef LL_CIRCULAR
    LINKEDLIST_TRAVERSE_UNTIL(linkedlist->head == node->next, node);
#else
    LINKEDLIST_TRAVERSE_UNTIL(END, pnode);
#endif
    
    new_node->next = pnode->next;
    pnode->next = new_node;

    return LL_OK;
}

ll_status_t linkedlist_length(pll_node_t linkedlist, uint32_t * size)
{
    pll_node_t pnode_next;
    uint32_t length;

    pnode_next = linkedlist->next;
    length = 0;

    LINKEDLIST_TRAVERSE_UNTIL((++length), pnode_next);

    *size = length;

    return LL_OK;
}

ll_status_t linkedlist_node_get_index(pll_node_t linkedlist, pll_node_t * pnode, uint32_t index)
{
    *pnode = linkedlist->next;

    LINKEDLIST_TRAVERSE_UNTIL(index--, *pnode);
    
    if ((++index) != 0)
    {
        return LL_ERR_OUTSIDERANGE;
    }

    return LL_OK;
}
