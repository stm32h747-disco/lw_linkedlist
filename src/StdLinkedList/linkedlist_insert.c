#include "linkedlist.h"

#undef LL_SAFE_INSERT
#include "linkedlist_impl.h"

/**
 * \brief           Insert a new linkedlist node at the given index. The node
 *                  will be inserted a the specified index or at the end the 
 *                  linkedlist.
 * \param[in]       linkedlist: Linkedlist head handle
 * \param[in]       new_node: Pointer to node to insert
 * \param[in]       index: Index to insert the node. Index must be within the
 *                      size of the Linkedlist.
 * \return          LL_OK on success
 */
ll_status_t linkedlist_node_insert(pll_node_t linkedlist, pll_node_t new_node, uint32_t index)
{
    return linkedlist_node_insert_impl(linkedlist, new_node, index);
}