// Used for fixed size variable int32_t, etc
#include "stdint.h"
// Used for offsetof macro
#include "stddef.h"

#ifndef UTILS_LINKEDLIST_H
# define UTILS_LINKEDLIST_H

#ifndef container_of
    #define container_of(ptr, type, member) ( {               \
        const typeof( ((type *)0)->member) * __mptr = (ptr);     \
        (type *)( (char *)__mptr - offsetof(type, member) );})
#endif

#define END 1

// TODO: Add argument to select which element to traverse
#define LINKEDLIST_TRAVERSE_UNTIL(additional_conditions, pnode) do {\
        while ((additional_conditions) && (pnode)->next != NULL) (pnode) = (pnode)->next; \
    } while (0)

typedef enum
{
    LL_OK = 0,
    LL_ERR_OUTSIDERANGE = 1,
    LL_ERR_NODENOTFOUND = 2,
    __LL_ALIGNMENT = UINT8_MAX
} ll_status_t;

#endif  /* UTILS_LINKEDLIST_H */