
#include "linkedlist.h"
#include "unity.h"

/* sometimes you may want to get at local data in a module.
 * for example: If you plan to pass by reference, this could be useful
 * however, it should often be avoided */
extern int Counter;

// Linked list of 5 nodes
ll_node_t   node4   = { .next = NULL };
ll_node_t   node3   = { .next = &node4 };
ll_node_t   node2   = { .next = &node3 };
ll_node_t   node1   = { .next = &node2 };
ll_node_t   node0   = { .next = &node1 };
ll_node_t   llist   = { .next = &node0 };

pll_node_t  pnode4  = &node4;
pll_node_t  pnode3  = &node3;
pll_node_t  pnode2  = &node2;
pll_node_t  pnode1  = &node1;
pll_node_t  pnode0  = &node0;
pll_node_t  pllist  = &llist;

void setUp(void)
{
     /* This is run before EACH TEST */

    pnode4->next = NULL;
    pnode3->next = pnode4;
    pnode2->next = pnode3;
    pnode1->next = pnode2;
    pnode0->next = pnode1;
    pllist->next = pnode0;
}

void tearDown(void)
{
}

void test_Linkedlist_RootInsert(void)
{
    ll_node_t new_node = { NULL };
    ll_status_t status = LL_ERR_OUTSIDERANGE;

    status = linkedlist_node_insert_head(pllist, &new_node);

    TEST_ASSERT_MESSAGE(&new_node == llist.next, "Root node insertion failed!");
    TEST_ASSERT_EQUAL_UINT32_MESSAGE(LL_OK, status, "Incorrect status returned!");
}

void test_Linkedlist_InsertIndex0(void)
{
    ll_status_t status = LL_ERR_OUTSIDERANGE;
    ll_node_t new_node = { NULL };

    status = linkedlist_node_insert(pllist, &new_node, 0);


    TEST_ASSERT_MESSAGE(&new_node != pnode4->next, "Node insertion failed! Node inserted at index 5 instead of index 0");
    TEST_ASSERT_MESSAGE(&new_node != pnode3->next, "Node insertion failed! Node inserted at index 4 instead of index 0");
    TEST_ASSERT_MESSAGE(&new_node != pnode2->next, "Node insertion failed! Node inserted at index 3 instead of index 0");
    TEST_ASSERT_MESSAGE(&new_node != pnode1->next, "Node insertion failed! Node inserted at index 2 instead of index 0");
    TEST_ASSERT_MESSAGE(&new_node != pnode0->next, "Node insertion failed! Node inserted at index 1 instead of index 0");
    TEST_ASSERT_MESSAGE(&new_node == pllist->next, "Node insertion failed using insert at index 0!");
    TEST_ASSERT_EQUAL_UINT32_MESSAGE(LL_OK, status, "Incorrect status returned!");
}

void test_Linkedlist_InsertIndex1(void)
{
    // Status hold for operations
    volatile ll_status_t status = LL_ERR_OUTSIDERANGE;

    // New node for insertion
    ll_node_t new_node = { NULL };

    status = linkedlist_node_insert(pllist, &new_node, 1);

    TEST_ASSERT_MESSAGE(&new_node != pnode4->next, "Node insertion failed! Node inserted at index 5 instead of index 1");
    TEST_ASSERT_MESSAGE(&new_node != pnode3->next, "Node insertion failed! Node inserted at index 4 instead of index 1");
    TEST_ASSERT_MESSAGE(&new_node != pnode2->next, "Node insertion failed! Node inserted at index 3 instead of index 1");
    TEST_ASSERT_MESSAGE(&new_node != pnode1->next, "Node insertion failed! Node inserted at index 2 instead of index 1");
    TEST_ASSERT_MESSAGE(&new_node != pllist->next, "Node insertion failed! Node inserted at index 0 instead of index 1");
    TEST_ASSERT_MESSAGE(&new_node == node0.next, "Node insertion failed using insert at index 1! Node has ot been inserted anywhere!");
    TEST_ASSERT_MESSAGE(LL_OK == status, "Incorrect status returned!");
    TEST_MESSAGE("Node inserted at correct index!");
}

void test_Linkedlist_InsertIndex3(void)
{
    // Status hold for operations
    volatile ll_status_t status = LL_ERR_OUTSIDERANGE;

    // New node for insertion
    ll_node_t new_node = { NULL };

    status = linkedlist_node_insert(pllist, &new_node, 3);

    TEST_ASSERT_MESSAGE(&new_node != pnode4->next, "Node insertion failed! Node inserted at index 5 instead of index 3");
    TEST_ASSERT_MESSAGE(&new_node != pnode3->next, "Node insertion failed! Node inserted at index 4 instead of index 3");
    TEST_ASSERT_MESSAGE(&new_node != pnode1->next, "Node insertion failed! Node inserted at index 2 instead of index 3");
    TEST_ASSERT_MESSAGE(&new_node != pnode0->next, "Node insertion failed! Node inserted at index 1 instead of index 3");
    TEST_ASSERT_MESSAGE(&new_node != pllist->next, "Node insertion failed! Node inserted at index 0 instead of index 3");
    TEST_ASSERT_MESSAGE(&new_node == pnode2->next, "Node insertion failed using insert at index 3! Node has ot been inserted anywhere!");
    TEST_ASSERT_MESSAGE(LL_OK == status, "Incorrect status returned!");
    TEST_MESSAGE("Node inserted at correct index!");
}

void test_Linkedlist_InsertIndexEnd(void)
{
    // Status hold for operations
    volatile ll_status_t status = LL_ERR_OUTSIDERANGE;

    // New node for insertion
    ll_node_t new_node = { NULL };

    status = linkedlist_node_insert(pllist, &new_node, 5);

    TEST_ASSERT_MESSAGE(&new_node != pnode3->next, "Node insertion failed! Node inserted at index 4 instead of index 5 (end)");
    TEST_ASSERT_MESSAGE(&new_node != pnode2->next, "Node insertion failed! Node inserted at index 3 instead of index 5 (end)");
    TEST_ASSERT_MESSAGE(&new_node != pnode1->next, "Node insertion failed! Node inserted at index 2 instead of index 5 (end)");
    TEST_ASSERT_MESSAGE(&new_node != pnode0->next, "Node insertion failed! Node inserted at index 1 instead of index 5 (end)");
    TEST_ASSERT_MESSAGE(&new_node != pllist->next, "Node insertion failed! Node inserted at index 0 instead of index 5 (end)");
    TEST_ASSERT_MESSAGE(&new_node == pnode4->next, "Node insertion failed using insert at index 5 (end)! Node has ot been inserted anywhere!");
    TEST_ASSERT_MESSAGE(LL_OK == status, "Incorrect status returned!");
    TEST_MESSAGE("Node inserted at correct index!");
}

void test_Linkedlist_InsertIndexBeyondEnd(void)
{
    // Status hold for operations
    volatile ll_status_t status = LL_ERR_OUTSIDERANGE;

    // New node for insertion
    ll_node_t new_node = { NULL };

    status = linkedlist_node_insert(pllist, &new_node, 7);

    TEST_ASSERT_MESSAGE(&new_node != pnode3->next, "Node insertion failed! Node inserted at index 4 instead of index 5 (end)");
    TEST_ASSERT_MESSAGE(&new_node != pnode2->next, "Node insertion failed! Node inserted at index 3 instead of index 5 (end)");
    TEST_ASSERT_MESSAGE(&new_node != pnode1->next, "Node insertion failed! Node inserted at index 2 instead of index 5 (end)");
    TEST_ASSERT_MESSAGE(&new_node != pnode0->next, "Node insertion failed! Node inserted at index 1 instead of index 5 (end)");
    TEST_ASSERT_MESSAGE(&new_node != pllist->next, "Node insertion failed! Node inserted at index 0 instead of index 5 (end)");
    TEST_ASSERT_MESSAGE(&new_node == pnode4->next, "Node insertion failed using insert at index 5 (end)! Node has ot been inserted anywhere!");
    TEST_ASSERT_MESSAGE(LL_OK == status, "Incorrect status returned!");
    TEST_MESSAGE("Node inserted at correct index!");
}

void test_Linkedlist_InsertIndexPreciseBeyondEnd(void)
{
    // Status hold for operations
    volatile ll_status_t status = LL_OK;

    // New node for insertion
    ll_node_t new_node = { NULL };

    status = linkedlist_node_insert_precise(pllist, &new_node, 7);

    TEST_ASSERT_MESSAGE(&new_node != pnode4->next, "Node insertion failed! Node inserted at index 5 (end) instead of failing");
    TEST_ASSERT_MESSAGE(&new_node != pnode3->next, "Node insertion failed! Node inserted at index 4 instead of failing");
    TEST_ASSERT_MESSAGE(&new_node != pnode2->next, "Node insertion failed! Node inserted at index 3 instead of failing");
    TEST_ASSERT_MESSAGE(&new_node != pnode1->next, "Node insertion failed! Node inserted at index 2 instead of failing");
    TEST_ASSERT_MESSAGE(&new_node != pnode0->next, "Node insertion failed! Node inserted at index 1 instead of failing");
    TEST_ASSERT_MESSAGE(&new_node != pllist->next, "Node insertion failed! Node inserted at index 0 instead of failing");
    TEST_ASSERT_MESSAGE(LL_ERR_OUTSIDERANGE == status, "Incorrect status returned!");
    TEST_MESSAGE("Node insertion failed correctly!");
}

void test_Linkedlist_InsertTail(void)
{    
    uint32_t index = 5;
    pll_node_t pnode_next;

    pnode_next = pllist->next;

    LINKEDLIST_TRAVERSE_UNTIL(index--, pnode_next);

    TEST_ASSERT_MESSAGE(index == 0, "Node index is not 0!");
}

int main(void) {
    UNITY_BEGIN();
    RUN_TEST(test_Linkedlist_RootInsert);
    RUN_TEST(test_Linkedlist_InsertIndex0);
    RUN_TEST(test_Linkedlist_InsertIndex1);
    RUN_TEST(test_Linkedlist_InsertIndex3);
    RUN_TEST(test_Linkedlist_InsertIndexEnd);
    RUN_TEST(test_Linkedlist_InsertIndexBeyondEnd);
    RUN_TEST(test_Linkedlist_InsertTail);
    RUN_TEST(test_Linkedlist_InsertIndexPreciseBeyondEnd);
    return UNITY_END();
}
