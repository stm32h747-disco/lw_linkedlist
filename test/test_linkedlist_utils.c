
#include "linkedlist.h"
#include "unity.h"

/* sometimes you may want to get at local data in a module.
 * for example: If you plan to pass by reference, this could be useful
 * however, it should often be avoided */
extern int Counter;

// Linked list of 5 nodes
ll_node_t   node4   = { .next = NULL };
ll_node_t   node3   = { .next = &node4 };
ll_node_t   node2   = { .next = &node3 };
ll_node_t   node1   = { .next = &node2 };
ll_node_t   node0   = { .next = &node1 };
ll_node_t   llist   = { .next = &node0 };

pll_node_t  pnode4  = &node4;
pll_node_t  pnode3  = &node3;
pll_node_t  pnode2  = &node2;
pll_node_t  pnode1  = &node1;
pll_node_t  pnode0  = &node0;
pll_node_t  pllist  = &llist;

void setUp(void)
{
     /* This is run before EACH TEST */

    pnode4->next = NULL;
    pnode3->next = pnode4;
    pnode2->next = pnode3;
    pnode1->next = pnode2;
    pnode0->next = pnode1;
    pllist->next = pnode0;
}

void tearDown(void)
{
}

void test_Linkedlist_Length(void)
{
    uint32_t length = 0;

    linkedlist_length(pllist, &length);

    TEST_ASSERT_MESSAGE(length != 0, "Linkedlist length is 0 incorrect!");
    TEST_ASSERT_MESSAGE(length != 1, "Linkedlist length is 1 incorrect!");
    TEST_ASSERT_MESSAGE(length != 2, "Linkedlist length is 2 incorrect!");
    TEST_ASSERT_MESSAGE(length != 3, "Linkedlist length is 3 incorrect!");
    TEST_ASSERT_MESSAGE(length != 4, "Linkedlist length is 4 incorrect!");
    TEST_ASSERT_MESSAGE(length != 6, "Linkedlist length is 6 incorrect!");
    TEST_ASSERT_MESSAGE(length != 7, "Linkedlist length is 7 incorrect!");
    TEST_ASSERT_MESSAGE(length == 5, "Linkedlist length is not 5!");
}

void test_Linkedlist_GetIndex0(void)
{
    pll_node_t pnode = NULL;
    ll_status_t status = LL_ERR_NODENOTFOUND;

    status = linkedlist_node_get_index(pllist, &pnode, 0);
    
    TEST_ASSERT_MESSAGE(pnode0 == pnode, "Node found is not index 0!");
    TEST_ASSERT_MESSAGE(status == LL_OK, "Incorrect status!");
}

void test_Linkedlist_GetIndex2(void)
{
    pll_node_t pnode = NULL;
    ll_status_t status = LL_ERR_NODENOTFOUND;

    status = linkedlist_node_get_index(pllist, &pnode, 2);
    
    TEST_ASSERT_MESSAGE(pnode2 == pnode, "Node found is not index 2!");
    TEST_ASSERT_MESSAGE(status == LL_OK, "Incorrect status!");
}

void test_Linkedlist_GetIndexEnd(void)
{
    pll_node_t pnode = NULL;
    ll_status_t status = LL_ERR_NODENOTFOUND;

    status = linkedlist_node_get_index(pllist, &pnode, 4);
    
    TEST_ASSERT_MESSAGE(pnode4 == pnode, "Node found is not index 4 (end)!");
    TEST_ASSERT_MESSAGE(status == LL_OK, "Incorrect status!");
}

int main(void) {
    UNITY_BEGIN();
    RUN_TEST(test_Linkedlist_Length);
    RUN_TEST(test_Linkedlist_GetIndex0);
    RUN_TEST(test_Linkedlist_GetIndex2);
    RUN_TEST(test_Linkedlist_GetIndexEnd);
    return UNITY_END();
}
