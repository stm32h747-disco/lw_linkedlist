
#include "linkedlist.h"
#include "unity.h"

/* sometimes you may want to get at local data in a module.
 * for example: If you plan to pass by reference, this could be useful
 * however, it should often be avoided */
extern int Counter;

// Linked list of 5 nodes
ll_node_t   node4   = { .next = NULL };
ll_node_t   node3   = { .next = &node4 };
ll_node_t   node2   = { .next = &node3 };
ll_node_t   node1   = { .next = &node2 };
ll_node_t   node0   = { .next = &node1 };
ll_node_t   llist   = { .next = &node0 };

pll_node_t  pnode4  = &node4;
pll_node_t  pnode3  = &node3;
pll_node_t  pnode2  = &node2;
pll_node_t  pnode1  = &node1;
pll_node_t  pnode0  = &node0;
pll_node_t  pllist  = &llist;

void setUp(void)
{
     /* This is run before EACH TEST */

    pnode4->next = NULL;
    pnode3->next = pnode4;
    pnode2->next = pnode3;
    pnode1->next = pnode2;
    pnode0->next = pnode1;
    pllist->next = pnode0;
}

void tearDown(void)
{
}

void test_Linkedlist_RemoveIndex0(void)
{
    // Status hold for operations
    volatile ll_status_t status = LL_ERR_OUTSIDERANGE;
    
    // Before
    // Index Node
    // 0     Node0
    // 1     Node1
    // 2     Node2
    // 3     Node3
    // 4     Node4

    status = linkedlist_node_remove_index(pllist, 0);

    // After
    // Index Node
    // 0     Node1
    // 1     Node2
    // 2     Node3
    // 3     Node4
    // 4     NULL
    
    TEST_ASSERT_MESSAGE(NULL == pnode0->next, "Node full removal failed! Node 1 should have NULL next");

    TEST_ASSERT_MESSAGE(pnode4 != pllist->next, "Node removal failed! Node 4 should not be at index 0");
    TEST_ASSERT_MESSAGE(pnode3 != pllist->next, "Node removal failed! Node 3 should not be at index 0");
    TEST_ASSERT_MESSAGE(pnode2 != pllist->next, "Node removal failed! Node 2 should not be at index 0");
    TEST_ASSERT_MESSAGE(pnode1 == pllist->next, "Node removal failed! Node 1 should be at index 0");

    TEST_ASSERT_MESSAGE(LL_OK == status, "Incorrect status returned!");
    TEST_MESSAGE("Node removed from correct index!");
}

void test_Linkedlist_RemoveIndex1(void)
{
    // Status hold for operations
    volatile ll_status_t status = LL_ERR_OUTSIDERANGE;
    
    // Before
    // Index Node
    // 0     Node0
    // 1     Node1
    // 2     Node2
    // 3     Node3
    // 4     Node4

    status = linkedlist_node_remove_index(pllist, 1);

    // After
    // Index Node
    // 0     Node0
    // 1     Node2
    // 2     Node3
    // 3     Node4
    // 4     NULL
    
    TEST_ASSERT_MESSAGE(NULL == pnode1->next, "Node full removal failed! Node 1 should have NULL next");

    TEST_ASSERT_MESSAGE(pnode4 != pnode0->next, "Node removal failed! Node 4 should not be at index 1");
    TEST_ASSERT_MESSAGE(pnode3 != pnode0->next, "Node removal failed! Node 3 should not be at index 1");
    TEST_ASSERT_MESSAGE(pnode2 == pnode0->next, "Node removal failed! Node 2 should be at index 1");
    TEST_ASSERT_MESSAGE(pnode1 != pnode0->next, "Node removal failed! Node 1 should not be at index 1");

    TEST_ASSERT_MESSAGE(LL_OK == status, "Incorrect status returned!");
    TEST_MESSAGE("Node removed from correct index!");
}

void test_Linkedlist_RemoveIndex3(void)
{
    // Status hold for operations
    volatile ll_status_t status = LL_ERR_OUTSIDERANGE;
    
    // Before
    // Index Node
    // 0     Node0
    // 1     Node1
    // 2     Node2
    // 3     Node3
    // 4     Node4

    status = linkedlist_node_remove_index(pllist, 3);

    // After
    // Index Node
    // 0     Node0
    // 1     Node1
    // 2     Node2
    // 3     Node4
    // 4     NULL
    
    TEST_ASSERT_MESSAGE(NULL == pnode3->next, "Node full removal failed! Node 1 should have NULL next");

    TEST_ASSERT_MESSAGE(pnode4 == pnode2->next, "Node removal failed! Node 4 should be at index 3");
    TEST_ASSERT_MESSAGE(pnode3 != pnode2->next, "Node removal failed! Node 3 should not be at index 3");
    TEST_ASSERT_MESSAGE(pnode2 != pnode2->next, "Node removal failed! Node 2 should not be at index 3");
    TEST_ASSERT_MESSAGE(pnode1 != pnode2->next, "Node removal failed! Node 1 should not be at index 3");

    TEST_ASSERT_MESSAGE(LL_OK == status, "Incorrect status returned!");
    TEST_MESSAGE("Node removed from correct index!");
}

void test_Linkedlist_RemoveIndexEnd(void)
{
    // Status hold for operations
    volatile ll_status_t status = LL_ERR_OUTSIDERANGE;
    
    // Before
    // Index Node
    // 0     Node0
    // 1     Node1
    // 2     Node2
    // 3     Node3
    // 4     Node4

    status = linkedlist_node_remove_index(pllist, 4);

    // After
    // Index Node
    // 0     Node0
    // 1     Node1
    // 2     Node2
    // 3     Node3
    // 4     NULL
    
    TEST_ASSERT_MESSAGE(NULL == pnode4->next, "Node full removal failed! Node 1 should have NULL next");

    TEST_ASSERT_MESSAGE(pnode4 != pnode3->next, "Node removal failed! Node 4 should not be at index 4 (end)");
    TEST_ASSERT_MESSAGE(pnode3 != pnode3->next, "Node removal failed! Node 3 should not be at index 4 (end)");
    TEST_ASSERT_MESSAGE(NULL == pnode3->next, "Node removal failed! Node 2 should be at index 4 (end)");
    TEST_ASSERT_MESSAGE(pnode1 != pnode3->next, "Node removal failed! Node 1 should not be at index 4 (end)");

    TEST_ASSERT_MESSAGE(LL_OK == status, "Incorrect status returned!");
    TEST_MESSAGE("Node removed from correct index!");
}

int main(void) {
    UNITY_BEGIN(); 
    RUN_TEST(test_Linkedlist_RemoveIndex1); 
    RUN_TEST(test_Linkedlist_RemoveIndex0);
    RUN_TEST(test_Linkedlist_RemoveIndex3);
    RUN_TEST(test_Linkedlist_RemoveIndexEnd);
    return UNITY_END();
}
